#pragma once

#include "auto/auto_meshes.h"

#define E_MESHES_OK 0
//OK
#define E_MESHES_UNKNOWN_COMMAND 1
//Command not recognized
#define E_MESHES_BAD_NAME 2
//Bad name
#define E_MESHES_CREATING 3
//Create failed
#define E_MESHES_MEMORY 4
//Not enought memory
#define E_MESHES_NO_SUCH_FILE 5
//File not found
#define E_MESHES_MAX 6
//Limit is reached
#define E_MESHES_EXISTS 7
//This name already exists
  
int auto_mesh_new(const char* name, const char* filename);
int auto_mesh_free(const char* name);
int meshes_command(const char* com);
int meshes_init();
int meshes_free();
