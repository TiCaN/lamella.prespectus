#pragma once

#include "auto/auto_shaders.h"
    
#define E_SHADERS_OK 0
  //OK
#define E_SHADERS_UNKNOWN_COMMAND 1
  //Command not recognized
#define E_SHADERS_BAD_NAME 2
  //Bad name
#define E_SHADERS_COMPILE 3
  //Compile error
#define E_SHADERS_MEMORY 4
  //Not enought memory
#define E_SHADERS_NO_SUCH_FILE 5
  //File not found
#define E_SHADERS_MAX 6
  //Limit is reached
#define E_SHADERS_LOCKED 7
  //Locked
#define E_SHADERS_LINK 8
  //Link error
#define E_SHADERS_EXISTS 9
  //Already exists
#define E_SHADERS_UNKNOWN_TYPE 10
  //Unknown type
#define E_SHADERS_NO_CONTEXT 11
  //Call lens_init first

int auto_shader_new(const char* name, const char* filename, int type);
int auto_shader_free(const char* name);

int auto_program_new(const char* name);
int auto_program_free(const char* name);

int auto_shaders_lock();
int auto_shaders_unlock();
int auto_shaders_free();
int auto_program_link(const char* name);
int auto_program_attach(const char* progname, const char* shadername);

int shaders_command(const char* com);
int shaders_init();
int shaders_free();
