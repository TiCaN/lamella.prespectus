#include "main.h"

#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include "shaders.h"
#include "meshes.h"
#include "lens.h"

lua_State* gLua = 0;

char* gFifoBuf;
int gQuit = 0;

int lua_quit(lua_State* s)
{
  gQuit = 1;
  return 0;
}

int lua_help(lua_State* s)
{
  meshes_lua_print_functions();
  shaders_lua_print_functions();
  lens_lua_print_functions();
  return 0;
}

int main(int argc, char** argv)
{
  gLua = luaL_newstate();
  if(!gLua) { printf("Lua failed!\n"); return 1; }
  lens_lua_register(gLua);
  meshes_lua_register(gLua);
  shaders_lua_register(gLua);

  int res;
  if(res = shaders_init())
  {
    shaders_error_print(res);
    goto exitmark;
  }
  if(res = meshes_init())
  {
    meshes_error_print(res);
    goto exitmark;
  }
  if(res = lens_init())
  {
    lens_error_print(res);
    goto exitmark;
  }

  lua_register(gLua, "quit", lua_quit);
  lua_register(gLua, "help", lua_help);

  gFifoBuf = malloc(2048);
  int fd;
  res = mkdir("/tmp/lamella", 0777);
  res = mkfifo("/tmp/lamella/prespectus", 0777);
  fd = open("/tmp/lamella/prespectus", O_RDONLY);
  if(fd <= 0)
  {
    printf("Starting failed!\n");
    return -1;
  }
  printf("Ready!\n");
  int reg = 0;
  while(1)
  {
    res = read(fd, gFifoBuf, 2047);
    if(res < 0)
    {
      close(fd);
      remove("/tmp/lamella/prespectus");
      return -2;
    }
    gFifoBuf[res] = 0;

    if(res)
    {
      if(luaL_dostring(gLua, gFifoBuf))
        printf("\033[31mLua error: \033[0m%s\n", lua_tostring(gLua, -1));
      if(gQuit) break;
    }

    lens_loop();
  }
exitmark:
  shaders_free();
  meshes_free();
  lens_free();
  close(fd);
  remove("/tmp/lamella/prespectus");
  free(gFifoBuf);
  return 0;
}
