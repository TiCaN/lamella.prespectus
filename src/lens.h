#pragma once

#include "auto/auto_lens.h"

#define E_LENS_OK 0
  //Ok
#define E_LENS_WINDOW 1
  //Creating window failed
#define E_LENS_TEMPORARY_CONTEXT 2
  //Temporary context failed
#define E_LENS_FORWARD_CONTEXT 3
  //Forward context failed
#define E_LENS_GLEW 4
  //GLEW failed

#define SS_EMPTY 0
#define SS_LOOP 1
extern int sdlState;

int auto_lens_init();
int auto_lens_free();

int lens_loop();
void lens_start();
void lens_finish();
void lens_free();
int lens_init();
