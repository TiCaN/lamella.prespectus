#include "shaders.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/gl.h>
#include "lens.h"

typedef struct
{
  char* name;
  GLuint shader;
} shader_t;

typedef struct
{
  char* name;
  GLuint program;
} shaders_t;

#define dMaxShaders 32
#define dMaxPrograms 128

int cSh = 0;
shader_t** sh = 0;
int cProg = 0;
shaders_t** prog = 0;

int shader_find(const char* name)
{
  for(int i = 0; i < cSh; ++i)
  {
    if(!strncmp(sh[i]->name, name, strlen(name)+1)) return i;
  }
  return -1;
}

int program_find(const char* name)
{
  for(int i = 0; i < cProg; ++i)
  {
    if(!strncmp(prog[i]->name, name, strlen(name)+1)) return i;
  }
  return -1;
}

int auto_shader_new(const char* name, const char* q_filename, int type)
{
  if(sdlState != SS_LOOP) return E_SHADERS_NO_CONTEXT;

  int result;

  lens_start();
  if(shader_find(name) != -1) { result =  E_SHADERS_EXISTS; goto RELEASE; }
  switch(type)
  {
  case 0: type = GL_VERTEX_SHADER; break;
  case 1: type = GL_FRAGMENT_SHADER; break;
  case 2: type = GL_GEOMETRY_SHADER; break;
  default: { result =  E_SHADERS_UNKNOWN_TYPE; goto RELEASE; }
  }
  if(cSh == dMaxShaders) { result = E_SHADERS_MAX; goto RELEASE; }
  char* src = 0;
  FILE* f = fopen(q_filename, "rb");
  if(!f) return E_SHADERS_NO_SUCH_FILE;
  fseek(f, 0, SEEK_END);
  int len = ftell(f);
  fseek(f, 0, SEEK_SET);
  src = malloc(len+1);
  fread(src, len, 1, f);
  fclose(f);
  src[len] = 0;
  shader_t* shader = malloc(sizeof(*shader));
  if(!shader) { result = E_SHADERS_MEMORY; goto RELEASE; }
  memset(shader, 0, sizeof(*shader));
  shader->shader = glCreateShader(type);
  glShaderSource(shader->shader, 1, (const GLchar**)&src, &len);
  glCompileShader(shader->shader);
  GLint success;
  GLchar log[2048];
  glGetShaderiv(shader->shader, GL_COMPILE_STATUS, &success);
  if(!success)
  {
    glGetShaderInfoLog(shader->shader, 2048, 0, log);
    printf("Shader error: %s\n", log);
    result = E_SHADERS_COMPILE;
    goto RELEASE;
  }
  len = strlen(name)+1;
  shader->name = malloc(len);
  memcpy(shader->name, name, len);
  sh[cSh] = shader;
  cSh++;
  result =  E_SHADERS_OK;

RELEASE:
  lens_finish();
  free(src);

  return result;
}

int auto_shader_free(const char* name)
{
  int shn = shader_find(name);
  if(shn == -1) return E_SHADERS_BAD_NAME;
  shader_t* shader = sh[shn];
  glDeleteShader(shader->shader);
  free(shader->name);
  free(shader);
  shader = sh[cSh-1];
  sh[shn] = shader;
  cSh--;
  return E_SHADERS_OK;
}

int auto_program_new(const char* name)
{
  if(program_find(name) != -1) return E_SHADERS_EXISTS;
  if(cProg == dMaxPrograms) return E_SHADERS_MAX;
  shaders_t* program = malloc(sizeof(*program));
  if(!program) return E_SHADERS_MEMORY;
  memset(program, 0, sizeof(*program));
  program->program = glCreateProgram();
  int len = strlen(name)+1;
  program->name = malloc(len);
  memcpy(program->name, name, len);
  prog[cProg] = program;
  cProg++;
  return E_SHADERS_OK;
}

int auto_program_free(const char* name)
{
  int progn = program_find(name);
  if(progn == -1) return E_SHADERS_BAD_NAME;
  shaders_t* program = prog[progn];
  glDeleteProgram(program->program);
  free(program->name);
  free(program);
  program = prog[cProg-1];
  prog[progn] = program;
  cProg--;
  return E_SHADERS_OK;
}

int auto_shaders_lock()
{
  return E_SHADERS_OK;
}

int auto_shaders_unlock()
{
  return E_SHADERS_OK;
}

int auto_shaders_free()
{
  return E_SHADERS_OK;
}

int auto_program_attach(const char* progname, const char* shadername)
{
  int progn = program_find(progname);
  int shan = shader_find(shadername);
  if((progn == -1) || (shan == -1)) return E_SHADERS_BAD_NAME;
  glAttachShader(prog[progn]->program, sh[shan]->shader);
  return E_SHADERS_OK;
}

int auto_program_link(const char* name)
{
  if(sdlState != SS_LOOP) return E_SHADERS_NO_CONTEXT;

  int result;
  lens_start();

  int progn = program_find(name);
  if(progn == -1) { result =  E_SHADERS_BAD_NAME; goto RELEASE; }
  glLinkProgram(prog[progn]->program);
  GLint success;
  GLchar log[2048];
  glGetProgramiv(prog[progn]->program, GL_LINK_STATUS, &success);
  if(!success)
  {
    glGetProgramInfoLog(prog[progn]->program, 2048, 0, log);
    printf("Link error: %s\n", log);
    result = E_SHADERS_LINK;
    goto RELEASE;
  }
  result = E_SHADERS_OK;

RELEASE:
  lens_finish();
  return result;
}

int shaders_init()
{
  sh = malloc(sizeof(*sh)*dMaxShaders);
  prog = malloc(sizeof(*sh)*dMaxPrograms);
  if(!sh || !prog) return E_SHADERS_MEMORY;
  return E_SHADERS_OK;
}

int shaders_free()
{
  if(sh)
  {
    free(sh);
    sh = 0;
  }
  if(prog)
  {
    free(prog);
    prog = 0;
  }
  return E_SHADERS_OK;
}
