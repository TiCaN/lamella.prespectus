#!/usr/bin/perl

use strict;

my $strsize = 1024;

$\ = $/ = "\n";

my $ERCL = "\e[31m"; #error
my $SCCL = "\e[32m"; #success
my $WNCL = ""; #warning
my $SPCL = "\e[0m"; #stop color output

my @SOURCES;
my $pathdir = "";

while(my $arg = shift @ARGV)
{
  if($arg =~ /^-h$|^--help$/)
  {
    print "srcgen.pl <list of *.c-files>";
    print "-p D,   --path D      use D as path";
    print "        --no-colors   disable color output";
    print "Tiberius Cautus Nubilus for project «Lamella» © 2018, GPL3";
    exit 0;
  } elsif($arg =~ /^--no-colors$/) {
    $ERCL = $SCCL = $WNCL = $SPCL = '';
  } elsif($arg =~ /^-p|--path$/) {
    $pathdir = shift @ARGV;
    chdir $pathdir;
  } elsif($arg =~ /^-/) {
    print "Unknown argumentum: $arg";
    exit 1;
  } else {
    push @SOURCES, $arg;
  }
}

mkdir "auto", 0777 if ! -e auto;

MAIN: for my $source (@SOURCES)
{
  $source =~ s/^$pathdir//;
  next MAIN if $source =~ /^-/;
  if($source !~ /\.c/)
  {
    print "$WNCL$source isn't a c-file, skip…$SPCL";
    next MAIN;
  }
  $source =~ /(.*)\.c/;
  $source = $1;
  my $upsource = $source;
  $upsource =~ tr/a-z/A-Z/;
 
  if(!open source, "<$source.h")
  {
    print "${ERCL}Reading $source.h failed!$SPCL";
    next MAIN;
  }
  
  my $code;
  {
    undef local $/;
    $code = <source>;
  }
  close source;

  if($code !~ /auto_/)
  {
    print "${WNCL}$source.c: auto functions not found, skip…$SPCL";
    next MAIN;
  }

  if(!open out, ">auto/auto_$source.c")
  {
    print "${WNCL}Writing auto_$source.c failed!$SPCL";
    next MAIN;
  }
  if(!open outh, ">auto/auto_$source.h")
  {
    print "${WNCL}Writing auto_$source.h failed!$SPCL";
    next MAIN;
  }

  print outh "#include \"../$source.h\"";
  print outh "#include <lua.h>";
  print outh "#include <lualib.h>";
  print outh "#include <lauxlib.h>";
  print outh '';

  print out "#include \"../$source.h\"";
  print out "#include <stdio.h>";
  print out "#include <stdlib.h>";
  print out "#include <string.h>";
  print out "#include <lua.h>";
  print out "#include <lualib.h>";
  print out "#include <lauxlib.h>";
  print out "#include \"auto_$source.h\"";
  print out '';

  print out "void ${source}_error_print(int error)";
  print outh "void ${source}_error_print(int error);";
  print out "{";
  print out "  switch(error)";
  print out "  {";
  while ($code =~ m!#define (E_\S*).*?//(.*?)$!msg)
  {
    print out "  case $1: printf(\"$source: $2\\n\"); break;";
  }
  print out "  }";
  print out "}";
  print out '';

  print out "const char* ${source}_error_to_str(int error)";
  print outh "const char* ${source}_error_to_str(int error);";
  print out "{";
  print out "  switch(error)";
  print out "  {";
  while ($code =~ m!#define (E_\S*).*?//(.*?)$!msg)
  {
    print out "  case $1: return \"$source: $2\";";
  }
  print out "  }";
  print out "}";
  print out '';

  if(!open source, "<$source.c")
  {
    print "Reading $source.c failed!";
    next MAIN;
  }

  {
    undef local $/;
    $code = <source>;
  }
  close source;

  my @reg;
  my @arglines;

  while ($code =~ /^int (auto_(\S*))\(([^\)]*)/msg)
  {
    my $function = $1;
    my $comand = "$2";
    push @arglines, $3;
    my @params = split(/, /, $3);
    $comand = "$comand " if scalar @params > 0;
    $comand =~ s/_/ /g;
    my $len = length($comand);

    $function =~ s/auto_//;

    push @reg, $function;

    print out "int ${source}_lua_${function}(lua_State* lua)";
    print outh "int ${source}_lua_${function}(lua_State* lua);";
    print out "{";

    my $iparam = -1;
    my $scaner = '';
    my $scanvar = '';

    PARAM: for my $param (@params)
    {
      $iparam++;
      my $luaparam = $iparam+1;
      if ($param =~ /const char\* /)
      {
        print out "  //string";
        print out "  const char* arg${iparam};";
        print out "  if(lua_isstring(lua, $luaparam))";
        print out "  {";
        print out "    arg${iparam} = lua_tostring(lua, ${luaparam});";
        print out "  } else {";
        print out "    lua_pushstring(lua, \"in function $function arg $luaparam should be string\");";
        print out "    lua_error(lua);";
        print out "    return 1;";
        print out "  }";
        $scaner = "$scaner%${strsize}s";
        $scanvar = "${scanvar}arg${iparam}, ";
        next PARAM;
      }
      if ($param =~ /int /)
      {
        print out "  //integer";
        print out "  int arg${iparam};";
        print out "  if(lua_isinteger(lua, $luaparam))";
        print out "  {";
        print out "    arg${iparam} = lua_tointeger(lua, ${luaparam});";
        print out "  } else {";
        print out "    lua_pushstring(lua, \"in function $function arg $luaparam should be integer\");";
        print out "    lua_error(lua);";
        print out "    return 1;";
        print out "  }";
        $scaner = "$scaner%d";
        $scanvar = "${scanvar}&arg${iparam}, ";
        next PARAM;
      }
      if ($param =~ /float /)
      {
        print out "  //float";
        print out "  float arg{$iparam};";
        print out "  if(lua_isnumber(lua, $luaparam))";
        print out "  {";
        print out "    arg${iparam} = lua_tonumber(lua, ${luaparam});";
        print out "  } else {";
        print out "    lua_pushstring(lua, \"in function $function arg $luaparam should be float\");";
        print out "    lua_error(lua);";
        print out "    return 1;";
        print out "  }";
        $scaner = "$scaner%f";
        $scanvar = "${scanvar}&arg${iparam}, ";
        next PARAM;
      }
    }
    $scanvar =~ s!, $!!;
    $iparam++;
    
    $function = "auto_$function(";
    if($iparam)
    {
      my $tmp = '';
      for(my $i = 0; $i < $iparam - 1; $i++)
      {
        $tmp = "${tmp}arg$i, ";
      }
      $iparam--;
      $tmp = "${tmp}arg$iparam";
      $iparam++;
      $function = "$function$tmp";
    }
    
    print out "  int res = $function);";
    print out "  if(res)";
    print out "  {";
    print out "    lua_pushstring(lua, ${source}_error_to_str(res));";
    print out "    lua_error(lua);";
    print out "  }";
    print out "  return 1;";
    print out "}\n";
  }

  print out "int ${source}_lua_register(lua_State* lua)";
  print outh "int ${source}_lua_register(lua_State* lua);";
  print out "{";
  for my $function (@reg)
  {
    my $luafunction = $function;
    $luafunction =~ s/^auto_//;
    print out "  lua_register(lua, \"$luafunction\", ${source}_lua_$function);";
  }
  print out "  return 0;";
  print out "}";
  print outh "void ${source}_lua_print_functions();";
  print out "void ${source}_lua_print_functions()";
  print out "{";
  {
    my $i = 0;
    for my $function (@reg)
    {
      my $luafunction = $function;
      $luafunction =~ s/^auto_//;
      print out "  printf(\"%s(%s)\\n\", \"$luafunction\", \"$arglines[$i]\");";
      $i++;
    }
  }
  print out "}";
  print "${SCCL}Source: $source.c$SPCL";
}
