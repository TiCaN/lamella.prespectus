#include "lens.h"

#include <stdio.h>
#include <stdlib.h>
#include <GL/glew.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <GL/gl.h>

SDL_Window* gWnd;
SDL_GLContext gContext;
int sdlState = SS_EMPTY;

int auto_lens_init()
{
  if(sdlState == SS_LOOP) return 0;

  SDL_Init(SDL_INIT_VIDEO | SDL_INIT_EVENTS | SDL_INIT_JOYSTICK | SDL_INIT_GAMECONTROLLER);

  gWnd = SDL_CreateWindow("Prespectus", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE);

  if(!gWnd)
  {
    printf("Error: %s\n", SDL_GetError());
    return E_LENS_WINDOW;
  }

  SDL_GLContext tcon = SDL_GL_CreateContext(gWnd);
  if(!tcon)
  {
    printf("Error: %s\n", SDL_GetError());
    return E_LENS_TEMPORARY_CONTEXT;
  }

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 4);

  gContext = SDL_GL_CreateContext(gWnd);

  if(!gContext)
  {
    printf("Error: %s\n", SDL_GetError());
    return E_LENS_FORWARD_CONTEXT;
  }

  SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 24);

  int res = glewInit();
  if(GLEW_OK != res)
  {
    printf("Error: GLEW failed: %s\n", glewGetErrorString(res));
    return E_LENS_GLEW;
  }

  SDL_GL_MakeCurrent(gWnd, gContext);
  glClearColor(1,0,1,1);
  SDL_GL_MakeCurrent(gWnd, 0);

  sdlState = SS_LOOP;

  return E_LENS_OK;
}

int lens_loop()
{
  if(sdlState != SS_LOOP) return 0;
  SDL_Event ev;
  if(SDL_PollEvent(&ev))
  {
    if(ev.type == SDL_QUIT){ sdlState = SS_EMPTY; return 0; }
  }
  SDL_GL_MakeCurrent(gWnd, gContext);
  glClear(GL_COLOR_BUFFER_BIT);

  

  SDL_GL_SwapWindow(gWnd);
  SDL_GL_MakeCurrent(gWnd, 0);

  return 0;
}

int auto_lens_free()
{
  if(sdlState == SS_EMPTY) return E_LENS_OK;
  SDL_GL_DeleteContext(gContext);
  SDL_DestroyWindow(gWnd);
  SDL_Quit();

  sdlState = SS_EMPTY;
  return E_LENS_OK;
}

void lens_start()
{
  if(sdlState == SS_LOOP) SDL_GL_MakeCurrent(gWnd, gContext);
}

void lens_finish()
{
  if(sdlState == SS_LOOP) SDL_GL_MakeCurrent(gWnd, 0);
}

int lens_init()
{
  return E_LENS_OK;
}

void lens_free()
{
  auto_lens_free();
}
