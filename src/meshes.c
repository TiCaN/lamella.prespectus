#include "meshes.h"

#include <stdio.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>
#include <GL/gl.h>

typedef struct
{
  char* name;
  float* vertices;
  int v_count;
  GLuint vbo;
} mesh_t;

#define dMaxMeshes 1000

int cMesh = 0;
mesh_t** mesh = 0;

int mesh_find(const char* name)
{
  for(int i = 0; i < cMesh; ++i)
  {
    if(!strncmp(mesh[i]->name, name, strlen(name)+1)) return i;
  }
  return -1;
}

int auto_mesh_new(const char* name, const char* q_filename)
{
  if(cMesh == dMaxMeshes) return E_MESHES_MAX;
  if(mesh_find(name) != -1) return E_MESHES_EXISTS;

  mesh_t* m = malloc(sizeof(*mesh));
  if(!m) return E_MESHES_MEMORY;
  memset(m, 0, sizeof(*m));

  FILE* f = fopen(q_filename, "rb");
  if(!f) return E_MESHES_NO_SUCH_FILE;
  fscanf(f, "%d", &m->v_count);
  
  m->vertices = malloc(sizeof(*m->vertices)*m->v_count*3);
  for(int i = 0; i < m->v_count*3; ++i)
    fscanf(f, "%f", &m->vertices[i]);
  
  glGenBuffers(1, &m->vbo);
  glBindBuffer(GL_ARRAY_BUFFER, m->vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(*m->vertices)*m->v_count*3, m->vertices, GL_STATIC_DRAW);
 
  int len = strlen(name)+1;
  m->name = malloc(len);
  memcpy(m->name, name, len);
  
  mesh[cMesh] = m;
  cMesh++;

  return E_MESHES_OK;
}

int auto_mesh_free(const char* name)
{
  int index = mesh_find(name);
  if(index == -1) return E_MESHES_BAD_NAME;
  mesh_t* m = mesh[index];
  if(m->vertices) free(m->vertices);
  free(m);
  cMesh--;
  return E_MESHES_OK;
}

int meshes_init()
{
  mesh = malloc(sizeof(*mesh)*dMaxMeshes);
  if(!mesh) return E_MESHES_MEMORY;
  return E_MESHES_OK;
}

int meshes_free()
{
  if(mesh)
  {
    free(mesh);
    mesh = 0;
  }
  return E_MESHES_OK;
}
